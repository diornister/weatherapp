# Weather Test

Test project for Stefanini, display the weather cast to specifc cities.

## Architecture
	Even though Xamarin Forms uses Model-View-ViewModel (MVVM), I opeted to modify it a little, to separate the Services,
that way the code would be more reusable.

## Third Party Libs

### Newtonsoft.Json 
	To fetch data from json files.

### sqlite-net-pcl
	To get access to SqLite Database.

## Refactor
	With more time it would be possible to try and add some tests.
	Modify the layout to have a better UX.

## Author

* **Diogenes Morais**