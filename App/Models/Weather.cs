﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Models
{
    public class Weather
    {
        public string Id { get; set; } = " ";
        public string Title { get; set; } = " ";
        public string DayWeather { get; set; } = " ";
        public string Img { get; set; } = " ";
        public string Temp { get; set; } = " ";
        public string TempMin { get; set; } = " ";
        public string TempMax { get; set; } = " ";
    }
}
