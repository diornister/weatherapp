﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using App.Models;
using Microsoft.CSharp.RuntimeBinder;

namespace App.Services
{
    public class Core
    {

        public static async Task<Weather> GetWeather(string id)
        {            
            string key = "2bac87e0cb16557bff7d4ebcbaa89d2f";
            string queryString = "http://api.openweathermap.org/data/2.5/weather?id="
                + id + "&appid=" + key;

            dynamic results = await DataService.getDataFromService(queryString).ConfigureAwait(false);

            if (results["weather"] != null)
            {
                Weather weather = new Weather();
                weather.Id = results["id"];
                weather.Title = (string)results["name"];
                weather.DayWeather = (string)results["weather"][0]["main"];
                weather.Img = (string)"http://openweathermap.org/img/w/" + results["weather"][0]["icon"] + ".png"; 
                weather.Temp = (string)results["main"]["temp"] + " F";
                weather.TempMin = (string)"Temp. Min.  "+results["main"]["temp_min"] + " F";
                weather.TempMax = (string)"Temp. Max.  " + results["main"]["temp_max"] + " F";
                
                return weather;
            }
            else
            {
                return null;
            }
        }
    }
}
