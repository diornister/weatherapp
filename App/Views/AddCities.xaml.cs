﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Forms;
using System.IO;

using Xamarin.Forms.Xaml;
using System.Collections.ObjectModel;
using App.Services;
using App.Models;

namespace App.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AddCities : ContentPage
	{
		public AddCities ()
		{
			InitializeComponent ();
		}

        private async void CitySelected(object sender, ItemTappedEventArgs e)
        {

            var city = (e.Item as City);            

            Weather weather = await Core.GetWeather(city.Id);

            var next = new Views.ShowCity
            {
                BindingContext = weather
            };

            await Navigation.PushAsync(next);

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            
            var assembly = IntrospectionExtensions.GetTypeInfo(typeof(AddCities)).Assembly;

            Stream stream = assembly.GetManifestResourceStream("App.cities.json");
            
            using (var reader = new System.IO.StreamReader(stream))
            {

                var json = reader.ReadToEnd();
                var list = JsonConvert.DeserializeObject<List<City>>(json);                                

                ObservableCollection<City> trends = new ObservableCollection<City>(list);
                listView.ItemsSource = trends;
            }
            
        }
    }
}