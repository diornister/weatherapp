﻿using App.Models;
using App.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App.Views
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            using( SQLite.SQLiteConnection conn = new SQLite.SQLiteConnection(App.DB_PATH))
            {
                List<Weather> weather = new List<Weather>();

                conn.CreateTable<City>();
                var cities = conn.Table<City>().ToList();

                foreach(var city in cities)
                {
                    Weather result = await Core.GetWeather(city.Id);
                    weather.Add(result);
                }

                listCities.ItemsSource = weather;
            }
        }

        private void OnCityFind(object sender, EventArgs e)
        {            
            Navigation.PushAsync(new AddCities());
        }

        private async void CitySelected(object sender, ItemTappedEventArgs e)
        {

            Weather city = (e.Item as Weather);            

            var next = new Views.ShowCity
            {
                BindingContext = city
            };

            await Navigation.PushAsync(next);

        }
    }
}
