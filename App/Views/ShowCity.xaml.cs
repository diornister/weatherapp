﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ShowCity : ContentPage
	{
        
		public ShowCity ()
		{
			InitializeComponent ();
		}

        private void AddCity(Object sender, EventArgs e)
        {
            City city = new City()
            {
                Id = CityId.Text,
                Name = CityName.Text
            };

            using (SQLite.SQLiteConnection conn = new SQLite.SQLiteConnection(App.DB_PATH))
            {
                conn.CreateTable<City>();

                var cities = conn.Query<City>("SELECT * FROM City WHERE Id = ?", CityId.Text).ToList();
                int count = cities.Count();

                if(count > 0)
                {
                    var delRows = conn.Query<City>("DELETE FROM City WHERE id = ?", CityId.Text).ToList();
                    int rows = delRows.Count();

                    if (rows > 0)
                        DisplayAlert("Error", "City not deleted.", "OK");
                    else
                        DisplayAlert("Success", "City deleted.", "OK");                    
                }
                else
                {
                    var rows = conn.Insert(city);

                    if (rows > 0)
                        DisplayAlert("Success", "City inserted.", "OK");
                    else
                        DisplayAlert("Error", "City not inserted.", "OK");
                }
            }
            
        }
    }
}